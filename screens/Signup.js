import { useNavigation } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../database/firebase";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";

export default function Signup() {
  const [email, onChangeEmail] = React.useState("");
  const [password, onChangePassword] = React.useState("");
  const [confirm, setConfirm] = React.useState("");
  const [isConfirm, setIsConfirm] = React.useState("");

  const onSubmit = async () => {
    if (confirm === password) {
      await createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          const user = userCredential.user;
          console.log(user);
          navigation.navigate("Homepage");
          setIsConfirm(true);
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          console.log(errorCode, errorMessage);
        });
    } else {
      setIsConfirm(false);
    }
  };

  const [visible, setVisible] = useState();

  const PassWarn = () => {
    console.log(password.length);
    if (password.length <= 5) {
      setVisible(true);
    } else if ((password.length = 0)) {
      setVisible(false);
    } else {
      setVisible(false);
    }

    return (
      <>
        {visible ? (
          <Text style={styles.warning}>
            Password must be greater than 6 character
          </Text>
        ) : null}
      </>
    );
  };

  const ConfirmWarn = () => {
    if (confirm === password) {
      setIsConfirm(false);
    } else {
      setIsConfirm(true);
    }

    return (
      <>
        {isConfirm ? (
          <Text style={styles.warning}>Password does not match</Text>
        ) : null}
      </>
    );
  };

  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View style={styles.loginContainer}>
        <Text style={styles.title}>Sign up</Text>
        <Text style={styles.subtitle}>Sign Up for a new account</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeEmail}
          placeholderTextColor={"#fff"}
          value={email}
          placeholder="Enter Email Address"
        />
        <TextInput
          style={styles.input}
          secureTextEntry={true}
          placeholder="Enter Password"
          placeholderTextColor={"#fff"}
          value={password}
          onChangeText={onChangePassword}
        />
        <PassWarn styles={styles.warning} />
        <TextInput
          style={styles.input}
          secureTextEntry={true}
          placeholder="Confirm Password"
          placeholderTextColor={"#fff"}
          value={confirm}
          onChangeText={setConfirm}
        />
        <ConfirmWarn styles={styles.warning} />
        <TouchableOpacity onPress={() => onSubmit()} style={styles.button}>
          <Text>Sign Up</Text>
        </TouchableOpacity>
        <Text style={styles.subtitle}>
          Already has an account?
          <Text style={styles.link} onPress={() => navigation.replace("Login")}>
            Log In!
          </Text>
        </Text>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    padding: 50,
    justifyContent: "center",
  },
  link: {
    color: "#81a1c1",
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    marginLeft: 2,
    marginTop: 5,
    width: 240,
    height: 40,
    borderRadius: 5,
    borderWidth: 2,
    backgroundColor: "#a3be8c",
    color: "#fff",
    borderColor: "#000",
  },
  warning: {
    color: "#bf616a",
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
    color: "#fff",
  },
  subtitle: {
    fontSize: 15,
    fontWeight: "normal",
    paddingBottom: 5,
    color: "#d8dee9",
  },
  input: {
    height: 40,
    borderRadius: 5,
    width: 240,
    margin: 5,
    marginLeft: 2,
    marginRight: 2,
    borderWidth: 1,
    padding: 10,
    color: "#d8dee9",
    borderColor: "#d8dee9",
  },
  container: {
    flex: 1,
    backgroundColor: "#0d0d0d",
    alignItems: "center",
    justifyContent: "center",
  },
});
