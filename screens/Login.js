import { StatusBar } from "expo-status-bar";
import { useState, useEffect } from "react";
import { onAuthStateChanged } from "firebase/auth";
import { useNavigation } from "@react-navigation/native";
import { auth } from "../database/firebase";
import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import Signup from "./Signup";
import { signInWithEmailAndPassword } from "firebase/auth";

export default function Login() {
  const [email, onChangeEmail] = React.useState("");
  const [password, onChangePassword] = React.useState("");
  const [passReq, setPassReq] = React.useState("");
  const navigation = useNavigation();

  const onLogin = () => {
    signInWithEmailAndPassword(auth, email, password)
      .then((usercredential) => {
        const user = usercredential.user;
        navigation.replace("Homepage");
      })
      .catch((error) => {
        const errorcode = error.code;
        const errormessage = error.message;
        console.log(errorcode, errormessage);
      })
      .catch((error) => {
        const errorcode = error.code;
        const errormessage = error.message;
        console.log(errorcode, errormessage);
      });
  };

  const [visible, setVisible] = useState();

  const PassWarn = () => {
    console.log(password.length);
    if (password.length <= 5) {
      setVisible(true);
    } else if ((password.length = 0)) {
      setVisible(false);
    } else {
      setVisible(false);
    }

    return (
      <>
        {visible ? (
          <Text style={styles.warning}>
            Password must be greater than 6 character
          </Text>
        ) : null}
      </>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.loginContainer}>
        <Text style={styles.title}>Log in</Text>
        <Text style={styles.subtitle}>Log in into your existing account</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeEmail}
          placeholderTextColor={"#fff"}
          value={email}
          placeholder="Enter Email Address"
        />
        <TextInput
          style={styles.input}
          placeholder="Enter Password"
          secureTextEntry={true}
          placeholderTextColor={"#fff"}
          value={password}
          onChangeText={onChangePassword}
        />
        <PassWarn />
        <TouchableOpacity
          onPress={() => {
            onLogin();
          }}
          style={styles.button}
        >
          <Text>Log in</Text>
        </TouchableOpacity>
        <View styles={styles.signup}>
          <Text style={styles.subtitle1}>
            Does not have an account?
            <Text
              style={styles.link}
              onPress={() => navigation.replace("Signup")}
            >
              Sign Up!
            </Text>
          </Text>
        </View>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  loginContainer: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    padding: 50,
    justifyContent: "center",
  },
  link: {
    color: "#81a1c1",
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    marginLeft: 2,
    marginTop: 5,
    width: 240,
    height: 40,
    borderRadius: 5,
    borderWidth: 2,
    backgroundColor: "#a3be8c",
    color: "#fff",
    borderColor: "#000",
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
    color: "#fff",
  },
  signup: {
    justifyContent: "space-between",
  },
  subtitle: {
    fontSize: 15,
    fontWeight: "normal",
    paddingBottom: 5,
    color: "#d8dee9",
  },
  subtitle1: {
    fontSize: 15,
    fontWeight: "normal",
    paddingBottom: 5,
    color: "#d8dee9",
  },
  input: {
    height: 40,
    borderRadius: 5,
    width: 240,
    margin: 5,
    marginLeft: 2,
    marginRight: 2,
    borderWidth: 1,
    padding: 10,
    color: "#d8dee9",
    borderColor: "#d8dee9",
  },
  warning: {
    color: "#bf616a",
  },
  container: {
    flex: 1,
    backgroundColor: "#0d0d0d",
    alignItems: "center",
    justifyContent: "center",
  },
});
