import React, { useState, useEffect } from "react";
import { onAuthStateChanged, signOut } from "firebase/auth";
import { Text, View, Button } from "react-native";
import { auth } from "../database/firebase";
import { useNavigation } from "@react-navigation/native";

const Home = () => {
  const navigation = useNavigation();
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        setLoggedIn(true);
      } else {
        setLoggedIn(false);
        navigation.replace("Login");
      }
    });
  }, []);

  const handleLogout = () => {
    signOut(auth);
    navigation.replace("Login");
  };

  return (
    <View>
      <Button
        title="logOut"
        onPress={() => {
          handleLogout();
        }}
      />
    </View>
  );
};

export default Home;
