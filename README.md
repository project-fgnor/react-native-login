# React Native Login & Sign Up system

The whole system is created ontop of firebase system.

# Get started

- Clone the repo 
```bash
npm install # install the dependencies
expo install
```

- Put your firebase configs from the console in database/firebase.js
- Add
```js
// Import 
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";


const firebaseConfig = {
  apiKey: "XXXXXXXX",
  authDomain: "XXXXXXXX",
  projectId: "XXXXXXXXXX",
  storageBucket: "XXXXXXXXXXXX",
  messagingSenderId: "XXXXXXXXXXX",
  appId: "XXXXXXXXXXXXXXXXx",
};

const app = initializeApp(firebaseConfig)
export const auth = getAuth(app)
```

